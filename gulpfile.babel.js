'use strict';

import fs from 'fs'
import path from 'path'
import mjmlEngine from 'mjml'
import browserSync from 'browser-sync'
import open from 'open'
import merge from 'merge-stream'
import gulp from 'gulp'
import mjml from 'gulp-mjml'
import plumber from 'gulp-plumber'
import imagemin from 'gulp-imagemin'
import sizereport from 'gulp-sizereport'
import zip from 'gulp-zip'
import index from 'gulp-index'
import replace from 'gulp-replace'
import ftp from 'vinyl-ftp' // upload assets to server via ftp protocol 
import sftp from "gulp-sftp"  // upload assets to server via sftp protocol



// preview server details
const server = {
    projectName: 'OCTOBEr DEALER EVENT 2020',
    baseFolder: 'fca/DLREVNT/2020/OCT',   
    baseCampaign:'DLREVNT',
    domain: 'preview.publicis.ca',
    host: '167.246.44.216',
    user: 'previewftp',
    password: 'ce5aw&thuTuS',
    projectSrcPath:'fca/DLREVNT/2020/OCT',
    folderName:'fca/DLREVNT/2020/OCT',
    projectDistributePath:'/data/www/vhosts/preview.publicis.ca/webroot/fca/DLREVNT/2020/OCT'  // need to use the path to the root
}

const assets = {
   domain: 'fcaassets.s3.ca-central-1.amazonaws.com',  
   projectDistributePath:'fca/DLREVNT/2020/OCT' 
     
}

 
 
 


// analytics snippets
const gaSnippet = '<img src = "https://www.google-analytics.com/collect?v=1&tid=UA-1341196-19&cid=555&aip=1&t=event&ec=email&ea=open&dp=%2Femail&dt='+server.baseCampaign+'" />'
// const litmusSnippet = `<style data-ignore-inlining>@media print{ #_t { background-image: url('https://fq9haiaa.emltrk.com/fq9haiaa?p&d=%%email%%');}} div.OutlookMessageHeader {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} table.moz-email-headers-table {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} blockquote #_t {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')} #MailContainerBody #_t {background-image:url('https://fq9haiaa.emltrk.com/fq9haiaa?f&d=%%email%%')}</style><div id="_t"></div>
// <img src="https://fq9haiaa.emltrk.com/fq9haiaa?d=%%email%%" width="1" height="1" border="0" alt="" />`


// index page options
const indexOpts = {
    'relativePath': './src',
    'prepend-to-output': () => `<head> <style>body{background: #D52536; background-image: url(pub.svg); background-repeat: no-repeat; background-position: center; background-size: auto 70%; font-family: system-ui; color: #fff; font-weight: 500; font-size: 16px; padding: 40px; line-height: 1em;}a{color: #fff;}</style></head><body>`,
    'title': server.projectName,
    'section-heading-template': () => '',
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filepath}</a></li>`
}
// index page for Preview Folder
const indexPreview = {
    'relativePath': './preview',
    'prepend-to-output': () => `<head> <style>body{background: #D52536; background-image: url(./pub.svg); background-repeat: no-repeat; background-position: center; background-size: auto 70%; font-family: system-ui; color: #fff; font-weight: 500; font-size: 16px; padding: 40px; line-height: 1em;}a{color: #fff;}</style></head><body>`,
    'title': server.projectName,
    'section-heading-template': () => '',
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filepath}</a></li>`
}
const indexDistribute = {
    'relativePath': './distribute',
    'prepend-to-output': () => `<head> <style>body{background: #D52536; background-image: url(pub.svg); background-repeat: no-repeat; background-position: center; background-size: auto 70%; font-family: system-ui; color: #fff; font-weight: 500; font-size: 16px; padding: 40px; line-height: 1em;}a{color: #fff;}</style></head><body>`,
    'title': server.projectName,
    'section-heading-template': () => '',
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filepath}</a></li>`
}
// in order to upload binary files to the remove server via sftp, the code in this file node_modules/gulp-sftp/index.js needs to be updated as below
const sftpUpdate = () =>{
    return function () {
        return gulp.src(`./node_modules/gulp-sftp/index.js`)
            .pipe(replace('file.pipe(stream); ', `if ( file.isStream() ) {
                file.contents.pipe( stream );
            } else if ( file.isBuffer() ) {
                stream.end( file.contents );
            }`))
            .pipe(gulp.dest(`./node_modules/gulp-sftp/index.js`))
    }
}

const getFolders = (dir) => {
    return fs.readdirSync(dir)
        .filter((file) => {
            return fs.statSync(path.join(dir, file)).isDirectory()
        })
}

const compileFolder = (folder) => {
    return function () {
        return gulp.src(`src/${folder}/*.mjml`)
            .pipe(plumber())
            .pipe(mjml(mjmlEngine, { minify: true, keepComments: false, validationLevel: 'strict' }))
            // .pipe(replace('</body>', `${litmusSnippet}${gaSnippet}</body>`))
            .pipe(replace('</body>', `${gaSnippet}</body>`))
            .pipe(gulp.dest(`./src/${folder}`))
    }
}

const compileAll = () => {
    let comp = getFolders('src').map((folder) => {
        return compileFolder(folder)()
    })
    return merge(comp)
} 

const createDistribute = () => {
    let imgMoveToDist = getFolders('src').map((folder) => {
        return gulp.src(`./src/${folder}/img/*`)
            .pipe(gulp.dest(`./distribute/${folder}/img`))
    })
    let mjmlToDist = getFolders('src').map((folder) => {
        return gulp.src(`./src/${folder}/*.mjml`)
            .pipe(gulp.dest(`./distribute/${folder}`))
    })
    let html = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
        .pipe(replace('src="img/', `src="https://${assets.domain}/${assets.projectDistributePath}/${folder}/img/`))
            .pipe(gulp.dest(`./distribute/${folder}`))
    })
    let logo = gulp.src('./pub.svg')
    .pipe(gulp.dest(`./distribute/`))
    return merge(imgMoveToDist,html,mjmlToDist)
}

const distributeList = () => {
    return gulp.src('./distribute/*/*.html')
        .pipe(sizereport({ '*': { 'maxSize': 80000 } }))
        .pipe(index(indexDistribute))
        .pipe(gulp.dest('./distribute'))
}

const list = () => {
    return gulp.src('./src/*/*.html')
        .pipe(sizereport({ '*': { 'maxSize': 80000 } }))
        .pipe(index(indexOpts))
        .pipe(gulp.dest('./src'))
}

const reload = (done) => {
    browserSync.reload()
    done()
}

const watchFiles = () => {
    browserSync.init({ server: 'src', baseDir: './src' })

    gulp.watch(['src/*', '!src/index.html'], gulp.series(list, reload))
    getFolders('src').forEach((folder) => {
        gulp.watch(`src/${folder}/*.mjml`, gulp.series(compileFolder(folder), reload))
    })
}

const images = () => {
    return gulp.src('src/**/*.+(jpg|png|gif)')
        .pipe(imagemin({ verbose: false }))
        .pipe(gulp.dest((file) => {
            return file.base
        }))
}



const compileOpen = (done) => {
    open(`src/index.html`)
    done()
}
 



// const distBuildZip = () => {
//     let zipFiles = getFolders('distribute').map((folder) => {
//         return gulp.src(`distribute/${folder}/**/*.+(html|jpg|png|gif)`)
//             .pipe(zip(`${folder}.zip`))
//             .pipe(gulp.dest('distDist'))
//     })
//     let zipSize = gulp.src('./distDist/*')
//         .pipe(sizereport({ '*': { 'maxSize': 1500000 } }))
//     return merge(zipFiles, zipSize)
// }



const previewFolder   = () => {
    let imgMoveToDist = getFolders('src').map((folder) => {
        return gulp.src(`./src/${folder}/img/*`)
            .pipe(gulp.dest(`./preview/${folder}/img`))
    })
    let html = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
        .pipe(replace('src="img/', `src="http://${server.domain}/${server.baseFolder}/${folder}/img/`))
        
            .pipe(gulp.dest(`./preview/${folder}`))
    })

    let logo = gulp.src('./pub.svg')
    .pipe(gulp.dest(`./preview/`))
     
   
    return merge(imgMoveToDist,html, logo)

}

const listPreview = () => {
    return gulp.src('./preview/*/*.html')
        .pipe(sizereport({ '*': { 'maxSize': 80000 } }))
        .pipe(index(indexPreview))
        .pipe(gulp.dest('./preview'))
}
// const previewUpload = () => {
    // const conn = ftp.create({ host: server.host, user: server.user, password: server.password })
    // let indexUpload = gulp.src(['./preview/index.html', './src/pub.svg'])
    //     .pipe(conn.dest(`${server.baseFolder}/`))

    // let htmlUpload = getFolders('preview').map((folder) => {
    //     return gulp.src(path.join('preview', folder, '*.html'))
    //         .pipe(conn.dest(`${server.baseFolder}/${folder}`))
    // })
    // let imgUpload = getFolders('preview').map((folder) => {
    //     return gulp.src(`./preview/${folder}/img/*`)
    //         .pipe(conn.dest(`${server.baseFolder}/${folder}/img`))
    // })
    // return merge(indexUpload, htmlUpload, imgUpload)
// }


const previewUpload = () => {
  
    return  gulp.src(`./preview/**/**`,)
            .pipe(sftp({
                 host: server.host,
                 user: server.user,
                pass: server.password,
                remotePath:server.projectDistributePath
        
            })
           
            )
}

 

const previewOpen = (done) => {
    open(`http://${server.domain}/${server.baseFolder}/`)
    done()
}
const View = (done) => {
    open(`https://${assets.domain}/${assets.projectDistributePath}/index.html`)
    done()
}
 
const watch = gulp.series(compileAll, list, watchFiles)
const preview = gulp.series(previewFolder, listPreview, previewUpload, previewOpen)
 
const distribute = gulp.series(createDistribute, list, distributeList)
 
const compile = gulp.series(compileAll, list,  compileOpen)
 
// const preview = gulp.series(gulp.parallel(compileAll, images),  list, distribute, previewUpload, previewOpen)
 
export { compile, watch, images,  distribute, View, preview, previewFolder, listPreview,previewOpen,previewUpload  }
export default watch            
// create html out of mjml use "gulp compile"
// create preview folder and upload assets to remove server use "gulp preview"
// create distribute folder with the assets having absolute path to the assets sever use 'gulp distribute" 
// only upload assets to the preview server use "gulp previewUpload"
// only open the preview server index page use "gulp preivewOpen"
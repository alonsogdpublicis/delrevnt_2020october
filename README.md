MJML validation is set to strict, keep an eye on the console to see compile errors.

## Gulp tasks
`compile`: compile mjml  
`watch`: *(default)* browsersync  
`images`: optimize images  
`test`: build litmus test files
`preview`: optimize images, compile mjml, upload to FTP  
`distribute`: optimize images, compile mjml, and zip into /dist folder  


This might come in handy:
~~~~
.phone-number-link {
    text-decoration: none;
    pointer-events: none;
}
@media only screen and (max-width: 599px) {
    .phone-number-link {
        text-decoration: underline !important;
        pointer-events: all !important;
    }
}
~~~~

// install gulp-sftp then find the index.js replace that line with the following: 


// start upload
if ( file.isStream() ) {
    file.contents.pipe( stream );
} else if ( file.isBuffer() ) {
    stream.end( file.contents );
}
